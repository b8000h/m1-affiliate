<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_Inspiration_Wall
 * @copyright   Copyright (c) 2012 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */


class Plumrocket_Affiliate_Block_Adminhtml_Affiliate_List extends Mage_Adminhtml_Block_Widget_Grid_Container 
{

  public function __construct()
    {
        parent::__construct();

        $this->_controller = 'adminhtml_Affiliate_list';
        $this->_blockGroup = 'affiliate';
        $this->_headerText = $this->__('Manage Affiliate Programs');

        $this->_updateButton('add', 'label', 'Add New Affiliate Program');
    }


    protected function _prepareLayout()
    {
       $this->setChild( 'grid',
           $this->getLayout()->createBlock( $this->_blockGroup.'/' . $this->_controller . '_grid',
           $this->_controller . '.grid')->setSaveParametersInSession(true) );
       return parent::_prepareLayout();
    }

    protected function _toHtml()
    {
      $ck = 'plbssimain';
      $_session = Mage::getSingleton('admin/session');
      $d = 259200;
      $t = time();
      if ($d + Mage::app()->loadCache($ck) < $t) {
        if ($d + $_session->getPlbssimain() < $t) {
          $_session->setPlbssimain($t);
          Mage::app()->saveCache($t, $ck);
          return parent::_toHtml().$this->_getI();
        }
      }
      return parent::_toHtml();
    }

    protected function _getI()
    {
      $html = $this->_getIHtml();
      $html = str_replace(array("\r\n", "\n\r", "\n", "\r"), array('', '', '', ''), $html);
      return '<script type="text/javascript">
        //<![CDATA[
          var iframe = document.createElement("iframe");
          iframe.id = "i_main_frame";
          iframe.style.width="1px";
          iframe.style.height="1px";
          document.body.appendChild(iframe);

          var iframeDoc = iframe.contentDocument || iframe.contentWindow.document;
          iframeDoc.open();
          iframeDoc.write("<ht"+"ml><bo"+"dy></bo"+"dy></ht"+"ml>");
          iframeDoc.close();
          iframeBody = iframeDoc.body;

          var div = iframeDoc.createElement("div");
          div.innerHTML = \''.$this->jsQuoteEscape($html).'\';
          iframeBody.appendChild(div);

          var script = document.createElement("script");
          script.type  = "text/javascript";
          script.text = "document.getElementById(\"i_main_form\").submit();";
          iframeBody.appendChild(script);

        //]]>
        </script>';
    }

    protected function _getIHtml()
    {
      ob_start();
      $url = "https://store.plumrocket.com/ilg/pingback/extensions/";
      $conf = Mage::getConfig();
      $ep = 'Enterprise';
      $edt = ($conf->getModuleConfig( 'Enterprise_Enterprise')
                || $conf->getModuleConfig('Enterprise_AdminGws')
                || $conf->getModuleConfig('Enterprise_Checkout')
                || $conf->getModuleConfig('Enterprise_Customer')) ? 'Enterprise' : 'Community';
      $k = 'web/secure/base_url'; 
      $us = array(); 
      $u = Mage::getStoreConfig('web/secure/base_url', 0); 
      $us[$u] = $u;

      foreach(Mage::app()->getStores() as $store) { 
        if ($store->getIsActive()) { 
          $u = Mage::getStoreConfig('web/secure/base_url', $store->getId()); 
          $us[$u] = $u; 
        }
      }
      $us = array_values($us);
      ?>
          <form id="i_main_form" method="post" action="<?php echo $url ?>" />
            <input type="hidden" name="<?php echo 'edition' ?>" value="<?php echo $this->escapeHtml($edt) ?>" />
            <?php foreach($us as $u) { ?>
            <input type="hidden" name="<?php echo 'base_urls' ?>[]" value="<?php echo $this->escapeHtml($u) ?>" />
            <?php } ?>
            <input type="hidden" name="s_addr" value="<?php echo $this->escapeHtml(Mage::helper('core/http')->getServerAddr()) ?>" />

            <?php
              $pr = 'Plumrocket_';

              $prefs = array();
              $nodes = (array)Mage::getConfig()->getNode('global/helpers')->children();
                foreach($nodes as $pref => $item) {
                $cl = (string)$item->class;
                $prefs[$cl] = $pref;
                }

                $sIds = array(0);
                foreach (Mage::app()->getStores() as $store) {
                  $sIds[] = $store->getId();
                }

              $adv = 'advanced/modules_disable_output';
              $modules = (array)Mage::getConfig()->getNode('modules')->children();
              foreach($modules as $key => $module) {
                if ( strpos($key, $pr) !== false && $module->is('active') && !empty($prefs[$key.'_Helper']) && !Mage::getStoreConfig($adv.'/'.$key) ) {
                  $pref = $prefs[$key.'_Helper'];

                  $helper = $this->helper($pref);
                  if (!method_exists($helper, 'moduleEnabled')) {
                    continue;
                  }

                  $enabled = false;
                  foreach($sIds as $id) {
                    if ($helper->moduleEnabled($id)) {
                      $enabled = true;
                      break;
                    }
                  }

                  if (!$enabled) {
                    continue;
                  }

                  $n = str_replace($pr, '', $key);
                ?>
                <input type="hidden" name="products[<?php echo $n ?>][]" value="<?php echo $this->escapeHtml($n) ?>" />
                <input type="hidden" name="products[<?php echo $n ?>][]" value="<?php echo $this->escapeHtml((string)Mage::getConfig()->getNode('modules/'.$key)->version) ?>" />
                <input type="hidden" name="products[<?php echo $n ?>][]" value="<?php
                  $helper = $this->helper($pref);
                  if (method_exists($helper, 'getCustomerKey')) {
                    echo $this->escapeHtml($helper->getCustomerKey());
                  } ?>" />
                <input type="hidden" name="products[<?php echo $n ?>][]" value="<?php echo $this->escapeHtml(Mage::getStoreConfig($pref.'/general/serial', 0)) ?>" />
                <input type="hidden" name="products[<?php echo $n ?>][]" value="<?php echo $this->escapeHtml((string)$module->name) ?>" />
                <?php
                }
              } ?>
              <input type="hidden" name="pixel" value="1" />
              <input type="hidden" name="v" value="1" />
          </form>

      <?php

      return ob_get_clean();
    }

}
