<?php 

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

@package    Plumrocket_Base-v1.x.x
@copyright  Copyright (c) 2014 Plumrocket Inc. (http://www.plumrocket.com)
@license    http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/


class Plumrocket_Base_Block_System_Config_Form_Field extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $product = Mage::getModel('plumbase/product')->loadByPref(str_replace('_general_serial', '',  $element->getHtmlId()));
        if ($product->isInStock()) {
            $ise = Mage::getConfig()->getModuleConfig('Enterprise_Checkout') && Mage::getConfig()->getModuleConfig('Enterprise_Checkout');
            $oldDesign = (version_compare('1.7.0', Mage::getVersion()) >= 0 && !$ise) || (version_compare('1.12.2', Mage::getVersion()) >= 0 && $ise);

            $src = 'images/success_msg_icon.gif';
            $title = 'Thank you! Your serial key is accepted. You can start using extension.';
            $html = '<div class="field-tooltip" style="background: url('.$this->getSkinUrl($src).') no-repeat 0 0; display: inline-block;width: 15px;height: 15px;position: relative;z-index: 1;vertical-align: middle;"><div '.( $oldDesign ? 'style="display:none;"' : '' ).'>'.$title.'</div></div>';
        } else {
            $html = '<img src="'.$this->getSkinUrl('images/error_msg_icon.gif').'" style="margin-top: 2px;float: right;" />';
        }

        return '<div style="width:300px">'.$element->getElementHtml() . $html.'</div>';
    }

}
