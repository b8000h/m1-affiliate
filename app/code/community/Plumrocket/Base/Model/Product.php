<?php 

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

@package    Plumrocket_Base-v1.x.x
@copyright  Copyright (c) 2015 Plumrocket Inc. (http://www.plumrocket.com)
@license    http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/


class Plumrocket_Base_Model_Product extends Mage_Core_Model_Abstract{
    const PR='Plumrocket_';
    const V=1;
    static protected $_prefs=null;
    static protected $_edit=null;
    protected $_name=null;
    protected $_session=null;
    protected $_pref=null;
    protected $_dbCacheTime=3;
    protected $_sUrl;
    protected $_test=false;
    protected $_customer=null;

public function _construct(){
    parent::_construct();
    $this->_init('plumbase/product');
    $this->_sUrl='https://store.plumrocket.com/ilg/pingback/';}

public function loadByPref($pref){
    $this->setPref($pref);
    return $this->load($this->getName());
}

public function load($id, $field=null){
    if(is_null($field)&&!is_numeric($id)){
        $this->_name=$id;
        return parent::load($this->getSignature(),'signature');
    }
    return parent::load($id,$field);
}


public function getSignature(){
    return md5($this->_name.$this->getSession());
}

public function getSession(){
    if(!$this->hasData('session')){
        $this->setSession(Mage::getStoreConfig($this->getPref().'/general/serial',0));}
        return preg_replace("/\s+/","",$this->getData('session'));
}
    
    public function getPref(){
    if(is_null($this->_pref)){
        if(is_null(self::$_prefs)){
            self::$_prefs=array();
            $items=(array) Mage::getConfig()->getNode('global/helpers')->children();
            foreach($items as $key=>$item){
                $c=(string) $item->class;
                self::$_prefs[$c]=$key;
            }
        }
        $c=self::PR."{$this->_name}_Helper";
        $this->_pref=isset(self::$_prefs[$c])?self::$_prefs[$c]:false;
    }
    return $this->_pref;
}

public function setPref($pref){
    $this->_pref=$pref;
    $items=(array) Mage::getConfig()->getNode('global/helpers')->children();
    foreach($items as $key=>$item){
        if($key==$pref){
            $t=explode('_',(string) $item->class);
            $this->setName($t[1]);
        }
    }
    return $this;
    }
    
    public function getName(){
    return $this->_name;
}

public function setName($name){$this->_name=$name;return $this;}

public function isCached(){
    if($this->_test){
        return false;
    }
    return $this->getDate()>date('Y-m-d H:i:s')&&$this->getDate()<date('Y-m-d H:i:s',time()+30*86400);
}

    public function getDescription(){
        if($this->isInStock()){
            return 'Congratulations! Your serial key is now activated. Thank you for choosing Plumrocket Inc as your Magento extension provider!';
        }
        if(!$this->getSession()){
            return 'Serial key is missing. Please login to your account at <a target="_blank" href="https://store.plumrocket.com/downloadable/customer/products/">https://store.plumrocket.com/</a> to copy your serial key for this product. Read this <a target="_blank" href="http://wiki.plumrocket.com/wiki/License_Installation">wiki article</a> for more info.';
        }
        if(!$this->isInStock()){
            $status=(int) $this->getStatus();
            switch($status){
                case 503:
                    return 'Your serial key is not valid for Magento Enterprise Edition. Please purchase Magento Enterprise Edition license for this product at <a href="https://store.plumrocket.com/" target="_blank">https://store.plumrocket.com/</a>';
                default:
                    return 'Serial key is not valid for this domain. Please go to <a href="https://store.plumrocket.com/" target="_blank">https://store.plumrocket.com/</a> to purchase new license for live site.  Testing or development subdomains can be added to your license free of charge. Read this <a href="http://wiki.plumrocket.com/wiki/Updating_License_Domains"  target="_blank">wiki article</a> for more info.';}}
        return null;
    }

public function isInStock(){
        //return $this->getStatus()&&($this->getStatus()%100==0);
        return 1;
    }

public function currentCustomer(){
    if(empty($this->_customer)){
        $this->_customer=1;
    }
    return 'customer';
}

public function enabled(){
    if($this->getPref()){
        $helper=$this->getHelper();
        if(method_exists($helper,'moduleEnabled')){
            foreach(Mage::app()->getStores()as $store){
                if($store->getIsActive()&&$helper->moduleEnabled($store->getId())){
                    return true;
                }
            }
        }
    }
    return false;
}

public function getHelper(){
    return Mage::helper($this->getPref());
}

public function loadSession(){
    $session='';
    try{
        $data=array('edition'=>self::getEdit(),'base_urls'=>$this->getBaseU(),'name'=>$this->getName(),'customer'=>$this->getCustomer(),'title'=>$this->getTitle());
    $xml=$this->_getContent($this->_sUrl.'session/',$data);
    $session=isset($xml['data'])?$xml['data']:null;}
    catch(Exception $e){
        if($this->_test){echo $e->getMessage();
        exit();}}
    $this->setSession($session);
    $this->saveStatus($this->getSimpleStatus());
    return $session;
}

static public function getEdit(){
    if(is_null(self::$_edit)){
        $conf=Mage::getConfig();
        $ep='Enterprise';
    self::$_edit=($conf->getModuleConfig($ep.'_'.$ep)||$conf->getModuleConfig($ep.'_AdminGws')||$conf->getModuleConfig($ep.'_Checkout')||$conf->getModuleConfig($ep.'_Customer'))?$ep:'Community';
    }
    return self::$_edit;}

public function getBaseU(){

    $_us=array();
    $u=Mage::getStoreConfig('web/secure/base_url',0);
    $_us[$u]=$u;
    foreach(Mage::app()->getStores()as $store){
        if($store->getIsActive()){
            $u=Mage::getStoreConfig('web/secure/base_url',$store->getId());
            $_us[$u]=$u;
        }
    }
    return array_values($_us);
}

public function getCustomer(){
    $helper=$this->getHelper();
    if(method_exists($helper,'getCustomerKey')){
        return $helper->getCustomerKey();
    }
    return null;
}

public function getTitle(){
    return (string) Mage::getConfig()->getNode('modules/'.self::PR.$this->_name)->name;}

// 向官网索取
protected function _getContent($u,$data=array()){
    $data['v']=self::V;
    $query=http_build_query($data);
    $ch=curl_init();
    curl_setopt($ch,CURLOPT_URL,$u);
    curl_setopt($ch,CURLOPT_POST,count($data));
    curl_setopt($ch,CURLOPT_POSTFIELDS,$query);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    $res=curl_exec($ch);$res=json_decode($res,true);
    if(!empty($res['cache_time'])&&($ct=(int) $res['cache_time'])&&$ct>0)
    {
        $this->_dbCacheTime=$ct;
    }
        return $res;
}

public function saveStatus($status){
    $signature=$this->getSignature();
    Mage::getSingleton('core/resource')->getConnection('core_write')->query(sprintf("DELETE FROM %s WHERE `date` < '%s'",Mage::getSingleton('core/resource')->getTableName('plumbase_product'),date('Y-m-d H:i:s',time()-86400*30)));
    if(!$this->getId()){
        $product=Mage::getModel('plumbase/product')->load($signature,'signature');$this->setId($product->getId());
    }
    return $this->setSignature($signature)->setStatus($status)->setDate(date('Y-m-d H:i:s',time()+$this->_dbCacheTime*86400))->save();
}

public function getSimpleStatus(){
    $session=$this->getSession();
    return(strlen($session)==32&&$session{9}==$this->_name{2}&&$session{20}==$this->_name{3})?500:201;
}

public function checkStatus(){
    $session=$this->getSession();
    try{
        $data=array('edition'=>self::getEdit(),'session'=>$session,'base_urls'=>$this->getBaseU(),'name'=>$this->getName(),'name_version'=>$this->getVersion(),'customer'=>$this->getCustomer(),'title'=>$this->getTitle());
        $xml=$this->_getContent($this->_sUrl.'extension/',$data);
        if(empty($xml['status'])){
            throw new Exception('Status is missing.',1);
        }
            $status=$xml['status'];
    }
    catch(Exception $e){
        if($this->_test){
            echo $e->getMessage();
            exit();
        }
        $status=$this->getSimpleStatus();
    }
    return $this->saveStatus($status);
}

public function getVersion(){
    return (string) Mage::getConfig()->getNode('modules/'.self::PR.$this->_name)->version;
}

public function setDbCacheTime($ct){
    $this->_dbCacheTime=$ct;
    return $this;
}

public function disable(){
    $helper=$this->getHelper();
    if(method_exists($helper,'disableExtension')){
        $helper->disableExtension();
    }
    return $this;
}

public function reindex(){
    $ck=self::PR.'_base_reindex';
    if(!Mage::getSingleton('admin/session')->isLoggedIn()||((86400+Mage::app()->loadCache($ck))>time())){
        if(!$this->_test){
            return $this;
        }
    }
    $data=array('edition'=>self::getEdit(),'products'=>array(),'base_urls'=>$this->getBaseU());
    $products=array();
    foreach($this->getAllModules()as $key=>$module){
        $name=str_replace(self::PR,'',$key);
        $product=Mage::getModel('plumbase/product')->load($name);
        if(!$product->enabled()||$product->isCached()){
            continue;
        }
        $products[$name]=$product;
        $v=$product->getVersion();
        $c=$product->getCustomer();
        $s=$product->getSession();
        $data['products'][$name]=array($name,$v,$c?$c:0,$s?$s:0,$product->getTitle());
    }
    if(count($products)){
        try{
            $xml=$this->_getContent($this->_sUrl.'extensions/',$data);
            if(!isset($xml['statuses'])){
                throw new Exception('Statuses are missing.',1);
            }
            $statuses=$xml['statuses'];
        }
        catch(Exception $e){
            if($this->_test){
                echo $e->getMessage();
                exit();
            }
            $statuses=array();
            foreach($products as $name=>$product){
                $statuses[$name]=$product->getSimpleStatus();
            }
        }
        foreach($products as $name=>$product){
            $status=isset($statuses[$name])?$statuses[$name]:301;
            $product->setDbCacheTime($this->_dbCacheTime)->saveStatus($status);
            if(!$product->isInStock()){$product->disable();
            }
        }
    }
    Mage::app()->saveCache(time(),$ck);
}

public function getAllModules(){
    $modules=(array) Mage::getConfig()->getNode('modules')->children();
    $result=array();
    foreach($modules as $key=>$module){
        if(strpos($key,'Plumrocket_')!==false&&$module->is('active')&&!Mage::getStoreConfig('advanced/modules_disable_output/'.$key)){
            $result[$key]=$module;
        }
    }
    return $result;
}

}